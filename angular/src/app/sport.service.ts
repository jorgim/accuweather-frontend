import { Injectable } from '@angular/core';
import {Observable, of } from 'rxjs';

import { Sport } from './sport';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { Day } from './day';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class SportService {

  constructor(private httpClient: HttpClient,
              private messageService: MessageService) { }

  getSports()
  {
    return this.httpClient.get<Sport[]>(environment.sportsUrl)
    .pipe(catchError(this.handleError('getSports')));
  }

  private handleError<T> (operation = 'operation', result?: any)
  {
    return (error: HttpErrorResponse): Observable<any> => {
      this.checkHttpStatus(error.status);
      return of(result as any);
    }
  }

  private checkHttpStatus(error)
  {
    console.log("entra a chequear el estado")
    switch(error)
    {
      case 404:
      this.messageService.addHttpError('Internal Problem: The Web APIs url is wrong');
      break;

      case 500:
      this.messageService.addHttpError('Internal Server error');
      break;

      case 503:
      this.messageService.addHttpError('The AccuWeathers API is down');
      break;

      default:
      this.messageService.addHttpError('There is a weird error, try again later');
      break;
    }
  }

  getAvailableDays(sportName: String, numberOfDays: number) : Observable<Day[]>
  {
    if(!this.isUnvalidField(numberOfDays))
    {
      const url = environment.daysUrl+sportName+'&numberOfDays='+numberOfDays;
      return this.httpClient.get<Day[]>(url).pipe(catchError(this.handleError('getAvailableDays')));
    }
  }

  private isUnvalidField (numberOfDays): boolean
  {
    if(isNaN(numberOfDays))
    {
      this.messageService.add('You must enter numbers, not letters');
      return true;
    }
    
    if (numberOfDays > 5 || numberOfDays < 1) 
    {
      this.messageService.add('You must enter a number between 1 and 5');
      return true;
    }
    
    return false;
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { Day } from '../day';
import { SportService} from '../sport.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-days',
  templateUrl: './list-days.component.html',
  styleUrls: ['./list-days.component.css']
})

export class ListDaysComponent implements OnInit {

  @Input() days: Day[];

  constructor(private location: Location,
              private sportService: SportService,
              private route: ActivatedRoute) {}

  ngOnInit() {}

  getColor(statusToPractice): boolean 
  {
    // if(statusToPractice === 'true')
    // {
    //   return true;
    // }
    
    // return false;

    return statusToPractice === 'true';
  }
}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService 
{
  messages: string[] = [];
  errorsLog: string[] = [];

  constructor() { }

  add(message: string)
  {
    if(this.messages.length > 0)
    {
      this.messages = [];
    }
    
    this.messages.push(message);
  }

  addHttpError(message: string)
  {
    if(this.errorsLog.length > 0)
    {
      this.errorsLog = [];
    }
    
    this.errorsLog.push(message);
  }

  clear(array=[])
  {
    this.messages  = [];
    this.errorsLog = [];
  }
}
